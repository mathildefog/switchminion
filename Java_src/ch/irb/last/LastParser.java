package ch.irb.last;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 Copyright 2017 - Mathilde Foglierini Perez
 This code is distributed open source under the terms of the GNU Free Documention License.

 This class parses a .maf file (LAST output alignment file) and creates a list of alignment AlignmentLastObject
 where the reference and the query are stored.
 */
public class LastParser {

    private File lastOutputFile;
    private ArrayList<AlignmentLastObject> lastAlignments = new ArrayList<>();

    public LastParser(File lastOutputFile) {
        this.lastOutputFile = lastOutputFile;
        try {
            parseFile();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void parseFile() throws FileNotFoundException {
        Scanner scanner = null;
        scanner = new Scanner(new FileReader(lastOutputFile));
        scanner.useDelimiter("a score=");
        // first use a Scanner to get each alignment
        loop:while (scanner.hasNext()) {
            int index = 0;
            SLastObject reference = null;
            SLastObject query = null;
            Scanner scan = new Scanner(scanner.next());
            scan.useDelimiter(Pattern.compile("([\n]|(\r\n))+")); //for each line
            while (scan.hasNext()) {
                String line = scan.next();
                if (line.matches("#.*")){
                    continue loop;
                }
                String[] cells = line.split("\\s+");
                if (index == 1) { // we have the reference seq
                    reference = new SLastObject(cells[1], cells[2], cells[3], cells[4], cells[5]);
                } else if (index == 2) { //we have the query sequence
                    query = new SLastObject(cells[1], cells[2], cells[3], cells[4], cells[5]);
                }
                index += 1;
            }
            if (query == null || reference == null) {
                System.out.println("Missing query or reference !!");
                System.exit(-1);
            }
            AlignmentLastObject align = new AlignmentLastObject(reference, query);
            lastAlignments.add(align);
            scan.close();
        }
        scanner.close();
    }


    public class SLastObject {
        private String id;
        private int start;
        private int alignLength;
        private int end;
        private String strand;
        private int seqLength;

        public SLastObject(String id, String start, String alignLength, String strand, String seqLength) {
            this.id = id;
            this.start = Integer.parseInt(start);
            this.alignLength = Integer.parseInt(alignLength);
            this.end = this.start + this.alignLength;
            this.strand = strand;
            this.seqLength = Integer.parseInt(seqLength);
        }

        public String getId() {
            return id;
        }

        public int getStart() {
            return start;
        }

        public int getAlignLength() {
            return alignLength;
        }

        public int getEnd() {
            return end;
        }

        public String getStrand() {
            return strand;
        }

        public int getSeqLength() {
            return seqLength;
        }
    }

    public class AlignmentLastObject {
        private SLastObject reference;
        private SLastObject query;

        public AlignmentLastObject(SLastObject reference, SLastObject query) {
            this.query = query;
            this.reference = reference;
        }

        public SLastObject getReference() {
            return reference;
        }

        public SLastObject getQuery() {
            return query;
        }
    }

    public ArrayList<AlignmentLastObject> getLastAlignments() {
        return lastAlignments;
    }
}
