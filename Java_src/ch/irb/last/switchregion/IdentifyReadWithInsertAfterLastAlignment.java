package ch.irb.last.switchregion;

import ch.irb.ManageFastaFiles.FastaFileMaker;
import ch.irb.last.LastParser;
import ch.irb.ManageFastaFiles.FastaFileParser;
import ch.irb.utils.Consts;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 Copyright 2017 - Mathilde Foglierini Perez
 This code is distributed open source under the terms of the GNU Free Documention License.
 This class process the LAST output file that aligned all MinION reads against the human genome hg19
 It will select the reads that fulfil the following criteria:
 - an insert sequence of mini 50bp (different chromosome than chr14)
 - with 2 flanking regions belonging to the Switch region (>= 100bp)
 - a gap <= 100bp is allowed between the insert and the switch
In output it wil create a bed and a tsv file with the insert coordinates and a fasta file with all reads
 that got an insert.
 */
public class IdentifyReadWithInsertAfterLastAlignment {

    private static String donor;
    private static File fastaFile;
    private static File lastOutFile;
    private int maxGapAllowed=100;
    private int startSwitch=106050000;
    private int endSwitch=106337000;
    private ArrayList<LastParser.AlignmentLastObject> lastAlignments = new ArrayList<>();
    private HashMap<String, MinionRead> idToMinionRead = new HashMap<>();
    private LinkedHashMap<String, String> selectedReadIdToSeq = new LinkedHashMap<>();
    private HashMap<String,ArrayList<String>>insertCoordToMinionReads = new HashMap<>();

    public static void main(String[] args) {
        if (args.length != 3) {
            System.out.println("ERROR: must have 3 arguments: for the donor, the fasta file and the last output file");
            System.exit(-1);
        }
        donor = args[0];
        fastaFile = new File(args[1]);
        lastOutFile = new File(args[2]);
        IdentifyReadWithInsertAfterLastAlignment ident = new IdentifyReadWithInsertAfterLastAlignment();
    }

    public IdentifyReadWithInsertAfterLastAlignment() {
        LastParser lastParser = new LastParser(lastOutFile);
        lastAlignments = lastParser.getLastAlignments();
        try {
            processData();
            writeReadWithInsertFastaFile();
            writeTsvFile();
            writeBedFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
    This method process the LAST alignment in order to identify the reads that are Switch/Insert/Switch
     */
    private void processData() throws IOException {
        //we store the fasta sequences
        //read fasta file
        FastaFileParser parser = new FastaFileParser(fastaFile);
        HashMap<String, String> completeIdToSeq = parser.getFastaIdToSequence();
        System.out.println("Retrieve minions read sequences " + completeIdToSeq.size());
        //we keep the shorter id
        HashMap<String, String> idToSeq = new HashMap<>();
        for (String completeId : completeIdToSeq.keySet()){
            String id = completeId.split("\\s+")[0];
            idToSeq.put(id,completeIdToSeq.get(completeId));
        }

        //we assign all alignment to all the minion reads
        for (LastParser.AlignmentLastObject align : lastAlignments) {
            String queryId = align.getQuery().getId();
            IdentifyReadWithInsertAfterLastAlignmentV2.MinionRead minionRead = new IdentifyReadWithInsertAfterLastAlignmentV2.MinionRead(queryId);
            if (idToMinionRead.containsKey(queryId)) {
                minionRead = idToMinionRead.get(queryId);
            }
            minionRead.addAlignment(align);
            idToMinionRead.put(queryId, minionRead);
        }
        System.out.println("Number of minion reads to process " + idToMinionRead.size());
        //we process minion reads
        IdLoop:
        for (String id : idToMinionRead.keySet()) {
            //System.out.println(">"+id);
            IdentifyReadWithInsertAfterLastAlignmentV2.MinionRead minionRead = idToMinionRead.get(id);
            String switchChromo = "chr14";
            int lastPosition = 0;
            String refChr = null;
            String otherChr = null;
            String chr;
            boolean left = false;
            boolean middle = false;
            boolean right = false;
            int matchedNucleotides = 0;
            int gap = 0;
            for (String range : minionRead.getRangeCoordToReference().keySet()) {
                //System.out.println(range);
                int start = Integer.parseInt(range.split("-")[0]);
                int end = Integer.parseInt(range.split("-")[1]);
                matchedNucleotides = end-start+1;
                String ref = minionRead.getRangeCoordToReference().get(range);
                int startRef = Integer.parseInt(ref.split(":")[1].split("-")[0]);
                int endRef = Integer.parseInt(ref.split(":")[1].split("-")[1]);
                if (refChr == null) { //begining of the seq must be switch
                    if (!ref.split(":")[0].equals(switchChromo)) {
                        //System.out.println("---REMOVE First chromo is not switch chr");
                        continue IdLoop;
                    }
                    if (matchedNucleotides>=100 && startRef >= startSwitch && endRef<= endSwitch) { //we want at least 100 bp of Switch on the left
                        refChr = ref.split(":")[0];
                        left = true;
                    }
                    //System.out.println("---LEFT start with chr" + refChr);
                } else {
                    chr = ref.split(":")[0];
                    if (!chr.equals(refChr)) { //change of chromo
                        //System.out.println("---change of chromo  chr" + chr + " matchnuc is " + matchedNucleotides + " and chr" + chr + " and gap " + gap);
                        gap = start - lastPosition;
                        if (left && !middle && matchedNucleotides >=50 && !chr.equals(switchChromo) && gap <= maxGapAllowed) {
                            middle = true;
                            //System.out.println("---MIDDLE with chr" + chr);
                            otherChr = ref;
                        } else if (left && middle && !right && matchedNucleotides>=100 && gap <= maxGapAllowed) { //end of insert
                            if (chr.equals(switchChromo) && startRef >= startSwitch && endRef<= endSwitch) {
                                right = true;
                                //System.out.println("---RIGHT start with chr" + chr);
                            }
                        }
                        refChr = chr;
                    }
                }
                lastPosition = end;
            }
            //If we have 1 read that fulfill the criteria we keep it
            if (left && middle && right) {
                selectedReadIdToSeq.put(id+"_"+otherChr, idToSeq.get(id));
                ArrayList<String> ids = new ArrayList<>();
                if (insertCoordToMinionReads.containsKey(otherChr)){
                    ids = insertCoordToMinionReads.get(otherChr);
                }
                ids.add(id);
                insertCoordToMinionReads.put(otherChr,ids);
            }
        }
        System.out.println("Number of insert coordinates "+insertCoordToMinionReads.size());
        System.out.println("Number of selected minion reads "+selectedReadIdToSeq.size());
    }

    private void writeReadWithInsertFastaFile() throws IOException {
        File fastaFile = new File(donor + "_selectedReadsAfterLAST.fasta");
        FastaFileMaker maker = new FastaFileMaker(fastaFile.getPath(), selectedReadIdToSeq);
    }

    private void writeTsvFile() throws IOException {
        BufferedWriter out = new BufferedWriter(new FileWriter(new File(donor+"_insertList_afterLAST.tsv")));
        for (String insert: insertCoordToMinionReads.keySet()){
            out.write(insert+"\t"+insertCoordToMinionReads.get(insert).size()+ Consts.ls);
        }
        out.close();
    }

    private void writeBedFile()throws IOException{
        BufferedWriter out = new BufferedWriter(new FileWriter(new File(donor+"_insertList_afterLAST.bed")));
        for (String insert: insertCoordToMinionReads.keySet()){
            out.write(insert.split(":")[0]+"\t"+insert.split(":")[1].split("-")[0]
                    +"\t"+insert.split(":")[1].split("-")[1]+"\t"+insertCoordToMinionReads.get(insert).size()+ Consts.ls);
        }
        out.close();
    }

    private class MinionRead {
        private String id;
        private LinkedHashMap<String, String> rangeCoordToReference = new LinkedHashMap<>();

        public MinionRead(String id) {
            this.id = id;
        }

        public void addAlignment(LastParser.AlignmentLastObject align) {
            LastParser.SLastObject query = align.getQuery();
            LastParser.SLastObject ref = align.getReference();
            int start = query.getStart();
            int end = query.getEnd();
            String refId = ref.getId() + ":" + ref.getStart() + "-" + ref.getEnd();
            rangeCoordToReference.put(start + "-" + end, refId);
        }

        public LinkedHashMap<String, String> getRangeCoordToReference() {
            return (LinkedHashMap<String, String>)sortByKeyAsc(rangeCoordToReference);
        }

        public  <K extends Comparable, V extends Comparable> Map<K, V> sortByKeyAsc(Map<K, V> map) {
            List<Map.Entry<K, V>> entries = new LinkedList<Map.Entry<K, V>>(map.entrySet());

            Collections.sort(entries, new Comparator<Map.Entry<K, V>>() {

               //we sort by the start coordinates!
                public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                    Integer st1=  new Integer(((String)o1.getKey()).split("-")[0]);
                    Integer st2=  new Integer(((String)o2.getKey()).split("-")[0]);
                    return st1.compareTo(st2);
                }
            });

            // LinkedHashMap will keep the keys in the order they are inserted
            // which is currently sorted on natural ordering
            Map<K, V> sortedMap = new LinkedHashMap<K, V>();

            for (Map.Entry<K, V> entry : entries) {
                sortedMap.put(entry.getKey(), entry.getValue());
            }

            return sortedMap;
        }
    }
}
